<!-- ECI:start added to schema JSON-LD -->
<script type="text/javascript">
	var eci_product_schema = {
		'product_name': '<?php echo (isset($heading_title)) ? $heading_title : ""; ?>',
		'product_description' : `<?php echo (isset($description)) ? strip_tags($description) : ""; ?>`,
		'product_price': <?php (isset($price)) ? echo str_replace('$','',$price) : ""; ?> 
	}
</script>
<!-- ECI:end added to schema JSON-LD -->